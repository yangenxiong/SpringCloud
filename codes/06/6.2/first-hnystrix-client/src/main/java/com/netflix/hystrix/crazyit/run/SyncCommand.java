package com.netflix.hystrix.crazyit.run;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

/**
 * 测试异步命令
 * 
 * @author 杨恩雄
 *
 */
public class SyncCommand extends HystrixCommand<String> {


	
	public SyncCommand() {
		// 设置执行超时时间为2秒
	    super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ExampleGroup"))
	            .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
	                   .withExecutionTimeoutInMilliseconds(2000)));
	    
	}

	@Override
	protected String run() throws Exception {
		System.out.println("这是同步命令");
		return "success";
	}

}
