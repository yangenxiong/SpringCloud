package com.netflix.hystrix;

import java.util.Map;
import java.util.concurrent.Future;

import com.netflix.hystrix.HystrixThreadPool.Factory;
import com.netflix.hystrix.crazyit.run.AsyncCommand;

public class AsyncTest {

	public static void main(String[] args) throws Exception {
		AsyncCommand c = new AsyncCommand();		
		// 异步执行，这将会把命令放到线程池排队
		Future<String> result = c.queue();		
		System.out.println("这句会先执行");
		// 调用get方法执行命令，完成后返回结果
		result.get();
		
		
		
	}

}
