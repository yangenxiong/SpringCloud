package org.crazyit.feign;

import feign.Feign;
import feign.jaxrs.JAXRSContract;

public class RSTest {

	public static void main(String[] args) {
		// 获取服务接口
		RSClient rsClient = Feign.builder()
				.contract(new JAXRSContract())
				.target(RSClient.class, "http://localhost:8080/");
		// 请求Hello World接口
		String result = rsClient.rsHello();
		System.out.println("    接口响应内容：" + result);
	}

}
